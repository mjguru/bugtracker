﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Constants
{
    public class ConstantRoleName
    {
        public const string COMPANY_ADMIN = "Company Admin";

        public const string COMPANY_OPERATOR = "Company Operator";

        public const string CLIENT = "Client";
    }
}
