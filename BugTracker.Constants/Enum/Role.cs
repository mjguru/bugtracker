﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Constants.Enum
{
   

    public enum Role
    {
        [Description(ConstantRoleName.COMPANY_ADMIN)]
        CompanyAdmin = 1,

        [Description(ConstantRoleName.COMPANY_OPERATOR)]
        CompanyOperator = 2,

        [Description(ConstantRoleName.CLIENT)]
        Client = 3,
    }
}
