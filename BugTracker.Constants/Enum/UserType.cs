﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Constants.Enum
{
    public enum UserType
    {
        [Description("Company")]
        Company = 1,

        [Description("Client")]
        Client = 2,
    }
}
