﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Constants
{
    public class ErrorConstant
    {
        public const string ERROR_OCCURED_CANNOT_PROCESS_REQUEST = "An error has occured. Could not process your request.";

        public const string EMAIL_EXISTS = "Email Address already exists.";

        public const string COMPANY_NAME_EXISTS = "Company Name already exists.";


        public const string INVALID_USERNAME_PASSWORD = "Sorry your credentials were not recognised.";

        public const string ACCOUNT_LOCKED = "Your account is locked for the next 15 minutes.";

        public const string OLD_PASSWORD_DOES_NOT_MATCH = "Old password does not match.";

        
    }
}
