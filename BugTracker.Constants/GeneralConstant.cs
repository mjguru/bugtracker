﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Constants
{
    public class GeneralConstant
    {
        public const string FORMS_AUTH_COOKIE_NAME = "BugTracker_Auth";
    }
}
