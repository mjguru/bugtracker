﻿using BugTracker.Database.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Data.Repository.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        BugTrackerEntity dbContext;

        public BugTrackerEntity Init()
        {
            return dbContext ?? (dbContext = new BugTrackerEntity());
        }

        protected override void DisposeCustomObject()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }

        
    }
}
