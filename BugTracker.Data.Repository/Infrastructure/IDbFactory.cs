﻿using BugTracker.Database.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Data.Repository.Infrastructure
{
    public interface IDbFactory : IDisposable
    {
        BugTrackerEntity Init();
    }
}
