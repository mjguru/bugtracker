﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using BugTracker.Database.Context;
using BugTracker.Database.Model;

namespace BugTracker.Data.Repository.Infrastructure
{
    public abstract class RepositoryBase<T> where T : class
    {
    
        private BugTrackerEntity dataContext;
        private readonly IDbSet<T> dbSet;

        protected IDbFactory DbFactory
        {
            get;
            private set;
        }

        protected BugTrackerEntity DbContext
        {
            get { return dataContext ?? (dataContext = DbFactory.Init()); }
        }
        

        protected RepositoryBase(IDbFactory dbFactory)
        {
            DbFactory = dbFactory;
            dbSet = DbContext.Set<T>();
        }

   
        public virtual void Add(T entity) 
        {
            dbSet.Add(entity);            
        }

        public virtual void Update(T entity)
        {            
            dbSet.Attach(entity);
            dataContext.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Delete(T entity)
        {
            dbSet.Remove(entity);
        }

        public virtual void Delete(Expression<Func<T, bool>> where)
        {
            List<T> objects = dbSet.Where<T>(where).ToList();
            foreach (T obj in objects)
                dbSet.Remove(obj);
        }

        public virtual T GetById(int id)
        {
            return dbSet.Find(id);
        }

        public virtual List<T> GetAll()
        {
            return dbSet.ToList();
        }

        public virtual List<T> GetMany(Expression<Func<T, bool>> where)
        {
            return dbSet.Where(where).ToList();
        }

        public IQueryable<T> GetAsIQueryable(Expression<Func<T, bool>> where)
        {
            return dbSet.Where(where);
        }

        public T GetOne(Expression<Func<T, bool>> where)
        {
            return dbSet.Where(where).FirstOrDefault<T>();

           
        }

        public int GetCount(Expression<Func<T, bool>> where)
        {
            return dbSet.Where(where).Count();


        }

    }
}
