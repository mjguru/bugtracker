﻿using BugTracker.Data.Repository.Infrastructure;
using BugTracker.Data.Repository.Repositories.Interface;
using BugTracker.Database.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Data.Repository.Repositories.Implementation
{
    public class ClientRepository : RepositoryBase<Client>, IClientRepository
    {
        public ClientRepository(IDbFactory dbFactory)
            : base(dbFactory) { }
        


        public IQueryable<Client> GetClientQueryByCompany(Guid companyIdentifier)
        {
            var clientListQuery = this.GetAsIQueryable(c => c.Company.Identifier == companyIdentifier && c.IsActive);
                        
            return clientListQuery;
        }
    }
}
