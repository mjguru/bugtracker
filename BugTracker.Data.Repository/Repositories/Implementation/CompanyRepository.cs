﻿using BugTracker.Data.Repository.Infrastructure;
using BugTracker.Data.Repository.Repositories.Interface;
using BugTracker.Database.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Data.Repository.Repositories.Implementation
{
    public class CompanyRepository : RepositoryBase<Company>, ICompanyRepository
    {
        public CompanyRepository(IDbFactory dbFactory)
            : base(dbFactory) { }


        public Company GetCompanyByName(string name)
        {
            var company = this.GetOne(c => c.Name.ToLower() == name.ToLower() && c.IsActive);
            return company;
        }

         
    }
}
