﻿using BugTracker.Data.Repository.Infrastructure;
using BugTracker.Data.Repository.Repositories.Interface;
using BugTracker.Database.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Data.Repository.Repositories.Implementation
{
    public class EmailRepository : RepositoryBase<Email>, IEmailRepository
    {
        public EmailRepository(IDbFactory dbFactory)
            : base(dbFactory) { }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Email> GetPendingEmailToSend()
        { 
            var list = this.GetMany(e => e.IsActive && e.IsSent == false);            
            return list;
        }

    }
}
