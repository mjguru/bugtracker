﻿using BugTracker.Data.Repository.Infrastructure;
using BugTracker.Data.Repository.Repositories.Interface;
using BugTracker.Database.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Data.Repository.Repositories.Implementation
{
    public class ForgotPasswordTokenRepository : RepositoryBase<ForgotPasswordToken>, IForgotPasswordTokenRepository
    {
        public ForgotPasswordTokenRepository(IDbFactory dbFactory)
            : base(dbFactory) { }


        public ForgotPasswordToken GetValidTokenByIdentifier(Guid indentifier)
        {
            DateTime currentDate = DateTime.Now;

            var forgotPasswordToken = this.GetOne(f => f.Identifier == indentifier && f.IsActive && f.ValidUntil >= currentDate);

            return forgotPasswordToken;
        }

        public List<ForgotPasswordToken> GetValidTokenByUser(Guid userIndentifier)
        {
            var forgotPasswordTokenList = this.GetMany(u => u.User.Identifier == userIndentifier && u.IsActive);
            return forgotPasswordTokenList;
        }
    }
}
