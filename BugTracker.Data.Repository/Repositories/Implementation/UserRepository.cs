﻿using BugTracker.Data.Repository.Infrastructure;
using BugTracker.Data.Repository.Repositories.Interface;
using BugTracker.Database.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Data.Repository.Repositories.Implementation
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(IDbFactory dbFactory)
            : base(dbFactory) { }

        public User GetUserByEmail(string email)
        {
            var user = this.GetOne(c => c.EmailAddress.ToLower() == email.ToLower() && c.IsActive);
            return user;
        }

        public User GetUserByIdentifier(Guid identifier)
        {
            var user = this.GetOne(u => u.Identifier == identifier && u.IsActive);
            return user;
        }

        public User GetUserByUnverfiedActivationLink(Guid activationIdentifier)
        {
            var user = this.GetOne(u => u.ActivationLinkIdentifier == activationIdentifier && u.IsActive && u.IsVerified == false);
            return user;
        }

        public List<User> GetUsersWithActivationEmailToSend()
        {
            var userList = this.GetMany(u => u.IsActive && u.IsActivationEmailSent == false);
            return userList;
        }

        public User GetVerifiedUserByEmail(string email)
        {
            var user = this.GetOne(u => u.EmailAddress.ToLower() == email.ToLower() && u.IsActive && u.IsVerified == true);
            return user;
        }
    }
}
