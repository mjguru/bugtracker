﻿using BugTracker.Data.Repository.Infrastructure;
using BugTracker.Database.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Data.Repository.Repositories.Interface
{
    public interface IUserRepository : IRepository<User>
    {
        User GetUserByEmail(string email);

        User GetVerifiedUserByEmail(string email);

        User GetUserByIdentifier(Guid identifier);

        User GetUserByUnverfiedActivationLink(Guid activationIdentifier);

        List<User> GetUsersWithActivationEmailToSend();
    }
}
