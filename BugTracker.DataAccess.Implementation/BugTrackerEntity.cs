﻿using BugTracker.Database.Context.MapAttribute;
using BugTracker.Database.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Database.Context
{
    public class BugTrackerEntity : DbContext
    {
        public BugTrackerEntity()
            : base("DatabaseConnectionString")
        {
            
        }

        public virtual void Commit()
        {
            base.SaveChanges();
        }

        public DbSet<User> User { get; set; }
        public DbSet<lkRole> lkRole { get; set; }
        public DbSet<lkUserType> lkUserType { get; set; }
        public DbSet<Company> Company { get; set; }
        public DbSet<Client> Client { get; set; }
        public DbSet<Email> Email { get; set; }
        public DbSet<ForgotPasswordToken> ForgotPasswordToken { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            new UserMap<User>(modelBuilder);
            new EmailMap<Email>(modelBuilder);
            new lkRoleMap<lkRole>(modelBuilder);
            new lkUserTypeMap<lkUserType>(modelBuilder);
            new CompanyMap<Company>(modelBuilder);
            new ClientMap<Client>(modelBuilder);
            new ForgotPasswordTokenMap<ForgotPasswordToken>(modelBuilder);

            
        }

    }
}
