﻿using BugTracker.Database.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Database.Context.MapAttribute
{
    public class CompanyMap<T> : PrimaryKeyBaseMap<T> where T : Company
    {
        public CompanyMap(DbModelBuilder modelBuilder) : base(modelBuilder)
        {
            var mapObject = modelBuilder.Entity<T>();

            mapObject.Property(t => t.Name).HasMaxLength(_MapConsts.NVARCHAR_250);

        }
    }
}
