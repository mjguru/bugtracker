﻿using BugTracker.Database.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Database.Context.MapAttribute
{
    public class ForgotPasswordTokenMap<T> : PrimaryKeyBaseMap<T> where T : ForgotPasswordToken
    {
        public ForgotPasswordTokenMap(DbModelBuilder modelBuilder) : base(modelBuilder)
        {
            var mapObject = modelBuilder.Entity<T>(); 


            //foreign key
            mapObject.HasRequired(r => r.User).WithMany().HasForeignKey(r => r.UserFK); 

        }
    }
}
