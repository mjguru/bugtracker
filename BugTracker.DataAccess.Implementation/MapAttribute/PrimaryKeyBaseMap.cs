﻿using BugTracker.Database.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Database.Context.MapAttribute
{
    public class PrimaryKeyBaseMap<T> where T : EntityBase
    {
        public PrimaryKeyBaseMap(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<T>().HasKey(m => m.Id).Property(m => m.Id).IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);


            modelBuilder.Entity<T>().Property(t => t.IsActive).IsRequired();

        }
    }
}
