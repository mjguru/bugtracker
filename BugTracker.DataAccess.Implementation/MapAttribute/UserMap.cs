﻿using BugTracker.Database.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Database.Context.MapAttribute
{
    public class UserMap<T> : PrimaryKeyBaseMap<T> where T : User
    {
        public UserMap(DbModelBuilder modelBuilder) : base(modelBuilder)
        {
            var mapObject = modelBuilder.Entity<T>();

            mapObject.Property(t => t.Firstname).HasMaxLength(_MapConsts.NVARCHAR_50);
            mapObject.Property(t => t.Lastname).HasMaxLength(_MapConsts.NVARCHAR_50);
            mapObject.Property(t => t.EmailAddress).HasMaxLength(_MapConsts.NVARCHAR_50);
            

            //foreign key
            mapObject.HasRequired(r => r.lkUserType).WithMany().HasForeignKey(r => r.UserTypeFK);
            mapObject.HasRequired(r => r.lkRole).WithMany().HasForeignKey(r => r.RoleFK);
            mapObject.HasOptional(r => r.Company).WithMany().HasForeignKey(r => r.CompanyFK);
            mapObject.HasOptional(r => r.Client).WithMany().HasForeignKey(r => r.ClientFK);

        }
    }
}
