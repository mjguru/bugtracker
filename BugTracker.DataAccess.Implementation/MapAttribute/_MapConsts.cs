﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Database.Context.MapAttribute
{
    public class _MapConsts
    {
        public const int NVARCHAR_50 = 50;

        public const int NVARCHAR_250 = 250;
    }
}
