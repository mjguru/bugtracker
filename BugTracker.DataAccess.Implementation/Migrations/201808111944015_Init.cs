namespace BugTracker.Database.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Client",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                        Address = c.String(),
                        Identifier = c.Guid(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        UpdatedBy = c.String(),
                        UpdatedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Company",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                        Address = c.String(),
                        Identifier = c.Guid(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        UpdatedBy = c.String(),
                        UpdatedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Email",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ToAddress = c.String(),
                        Body = c.String(),
                        Subject = c.String(),
                        IsSent = c.Boolean(nullable: false),
                        SentDate = c.DateTime(),
                        ErrorMessage = c.String(),
                        UserFK = c.Int(),
                        Identifier = c.Guid(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        UpdatedBy = c.String(),
                        UpdatedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.UserFK)
                .Index(t => t.UserFK);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ActivationLinkIdentifier = c.Guid(),
                        IsVerified = c.Boolean(nullable: false),
                        VerifiedOn = c.DateTime(),
                        CompanyFK = c.Int(),
                        ClientFK = c.Int(),
                        RoleFK = c.Int(nullable: false),
                        UserTypeFK = c.Int(nullable: false),
                        Firstname = c.String(maxLength: 50),
                        Lastname = c.String(maxLength: 50),
                        EmailAddress = c.String(maxLength: 50),
                        Salt = c.String(),
                        Password = c.String(),
                        NoOfIncorrectLoginAttempts = c.Int(nullable: false),
                        AccountLockedUntil = c.DateTime(),
                        IsLock = c.Boolean(nullable: false),
                        LastPasswordResetDate = c.DateTime(),
                        IsActivationEmailSent = c.Boolean(nullable: false),
                        ActivationEmailSentDate = c.DateTime(),
                        Identifier = c.Guid(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        UpdatedBy = c.String(),
                        UpdatedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Client", t => t.ClientFK)
                .ForeignKey("dbo.Company", t => t.CompanyFK)
                .ForeignKey("dbo.lkRole", t => t.RoleFK, cascadeDelete: true)
                .ForeignKey("dbo.lkUserType", t => t.UserTypeFK, cascadeDelete: true)
                .Index(t => t.CompanyFK)
                .Index(t => t.ClientFK)
                .Index(t => t.RoleFK)
                .Index(t => t.UserTypeFK);
            
            CreateTable(
                "dbo.lkRole",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Name = c.String(nullable: false, maxLength: 250),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.lkUserType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Name = c.String(nullable: false, maxLength: 250),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ForgotPasswordToken",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserFK = c.Int(nullable: false),
                        ValidUntil = c.DateTime(nullable: false),
                        Identifier = c.Guid(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        UpdatedBy = c.String(),
                        UpdatedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.UserFK, cascadeDelete: true)
                .Index(t => t.UserFK);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ForgotPasswordToken", "UserFK", "dbo.User");
            DropForeignKey("dbo.Email", "UserFK", "dbo.User");
            DropForeignKey("dbo.User", "UserTypeFK", "dbo.lkUserType");
            DropForeignKey("dbo.User", "RoleFK", "dbo.lkRole");
            DropForeignKey("dbo.User", "CompanyFK", "dbo.Company");
            DropForeignKey("dbo.User", "ClientFK", "dbo.Client");
            DropIndex("dbo.ForgotPasswordToken", new[] { "UserFK" });
            DropIndex("dbo.User", new[] { "UserTypeFK" });
            DropIndex("dbo.User", new[] { "RoleFK" });
            DropIndex("dbo.User", new[] { "ClientFK" });
            DropIndex("dbo.User", new[] { "CompanyFK" });
            DropIndex("dbo.Email", new[] { "UserFK" });
            DropTable("dbo.ForgotPasswordToken");
            DropTable("dbo.lkUserType");
            DropTable("dbo.lkRole");
            DropTable("dbo.User");
            DropTable("dbo.Email");
            DropTable("dbo.Company");
            DropTable("dbo.Client");
        }
    }
}
