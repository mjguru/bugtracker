namespace BugTracker.Database.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatedUserTabe : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.User", "LastActivityDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.User", "LastActivityDate");
        }
    }
}
