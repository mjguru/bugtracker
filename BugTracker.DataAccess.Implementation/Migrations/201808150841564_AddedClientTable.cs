namespace BugTracker.Database.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedClientTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Client", "CompanyFK", c => c.Int(nullable: false));
            CreateIndex("dbo.Client", "CompanyFK");
            AddForeignKey("dbo.Client", "CompanyFK", "dbo.Company", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Client", "CompanyFK", "dbo.Company");
            DropIndex("dbo.Client", new[] { "CompanyFK" });
            DropColumn("dbo.Client", "CompanyFK");
        }
    }
}
