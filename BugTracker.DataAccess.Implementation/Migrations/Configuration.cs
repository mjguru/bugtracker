namespace BugTracker.Database.Context.Migrations
{
    using BugTracker.Database.Model;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using BugTracker.Database.Context; 
    using BugTracker.Constants.Enum;
    using System.ComponentModel;
    using System.Reflection;

    internal sealed class Configuration : DbMigrationsConfiguration<BugTrackerEntity>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(BugTrackerEntity context)
        {
            //if (System.Diagnostics.Debugger.IsAttached == false)   System.Diagnostics.Debugger.Launch();


            PopulateDictionary(context);

            //Save changes in combine mode
            context.SaveChanges();
        }

        private void PopulateDictionary(BugTrackerEntity context)
        {
            SaveDictionaryFromEnum<lkRole>(typeof(Role), context);
            SaveDictionaryFromEnum<lkUserType>(typeof(Constants.Enum.UserType), context);
        }

        private void SaveDictionaryFromEnum<T>(Type e, BugTrackerEntity context) where T : DictionaryBase, new()
        {
            foreach (FieldInfo fi in e.GetFields())
            {
                object[] attrs =  fi.GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (attrs != null && attrs.Length > 0)
                {
                    foreach (DescriptionAttribute attr in attrs)
                    {
                        
                        string name = attr.Description;
                        if (!string.IsNullOrWhiteSpace(name))
                        {
                            var pocoTable = new T();

                            pocoTable.Id = (int)fi.GetRawConstantValue();
                            pocoTable.Name = name;
                            pocoTable.IsActive = true;

                            context.Set<T>().AddOrUpdate(pocoTable);
                        }
                    }
                }
            } 
        }
    }
}
