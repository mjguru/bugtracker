﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Database.Model
{
    public class Client : EntityBase
    {
        public string Name { get; set; }

        public string Address { get; set; }

        public int CompanyFK { get; set; }

        public virtual Company Company { get; set; }
    }
}
