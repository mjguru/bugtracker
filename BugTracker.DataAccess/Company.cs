﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Database.Model
{
    public class Company : EntityBase
    {
        public string Name { get; set; }

        public string Address { get; set; }
    }
}
