﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Database.Model
{
    public class DictionaryBase 
    {
        public int Id { get; set; }

        public bool IsActive { get; set; }

        public string Name { get; set; }
    }
}
