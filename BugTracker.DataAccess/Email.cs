﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Database.Model
{
    public class Email : EntityBase
    { 

        public string ToAddress { get; set; }

        public string Body { get; set; }

        public string Subject { get; set; }

        public bool IsSent { get; set; } 

        public DateTime? SentDate { get; set; }

        public string ErrorMessage { get; set; }

        public int? UserFK { get; set; }

        public virtual User User { get; set; }


    }
}
