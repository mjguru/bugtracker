﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Database.Model
{
    public class EntityBase : HistoryBase
    {
        public int Id { get; set; }

        public Guid Identifier { get; set; }

        public bool IsActive { get; set; }
    }
}
