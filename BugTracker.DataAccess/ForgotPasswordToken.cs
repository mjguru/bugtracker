﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Database.Model
{
    public class ForgotPasswordToken : EntityBase
    { 
        public int UserFK { get; set; }

        public DateTime ValidUntil { get; set; } 

        public virtual User User { get; set; }


    }
}
