﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Database.Model
{
    public class User : EntityBase
    { 

        public Guid? ActivationLinkIdentifier { get; set; }

        public bool IsVerified { get; set; }

        public DateTime? VerifiedOn { get; set; }

        public int? CompanyFK { get; set; }

        public int? ClientFK { get; set; }

        public int RoleFK { get; set; }

        public int UserTypeFK { get; set; }        

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string EmailAddress { get; set; }

        public string Salt { get; set; } 

        public string Password { get; set; }

        public int NoOfIncorrectLoginAttempts { get; set; }

        public DateTime? AccountLockedUntil { get; set; }

        public bool IsLock { get; set; }

        public DateTime? LastPasswordResetDate { get; set; }

        public DateTime? LastActivityDate { get; set; }

        public bool IsActivationEmailSent { get; set; }

        public DateTime? ActivationEmailSentDate { get; set; }


        public virtual lkRole lkRole { get; set; }

        public virtual lkUserType lkUserType { get; set; }

        public virtual Company Company { get; set; }

        public virtual Client Client { get; set; }


    }
}
