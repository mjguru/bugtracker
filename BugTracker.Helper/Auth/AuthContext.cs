﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BugTracker.Helper.Auth
{
    
    /// <summary>
    /// 
    /// </summary>
    public class AuthContext
    {
        public bool IsAuthenticated { get; private set; } = false;

        public Guid UserIdentifier { get; private set; }

        public string FirstName { get; private set; }

        public string LastName { get; private set; }

        public string Role { get; private set; }

        public Guid CompanyIdentifier { get; set; }

        public string CompanyName { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public AuthContext()
        {
           
            CustomPrincipal currentUser = HttpContext.Current.User as CustomPrincipal;

            if (currentUser != null)
            {
                IsAuthenticated = true;
                UserIdentifier = currentUser.UserIdentifier;
                FirstName = currentUser.FirstName;
                LastName = currentUser.LastName;
                Role = String.Join(",", currentUser.Roles);
                CompanyIdentifier = currentUser.CompanyIdentifier;
                CompanyName = currentUser.CompanyName;


            }
        }
    }
}
