﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Helper.Auth
{
    public class AuthenticationModel
    {
        public Guid UserIdentifier { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<string> RoleName { get; set; }

        public Guid CompanyIdentifier { get; set; }
        public string CompanyName { get; set; }

    }
}
