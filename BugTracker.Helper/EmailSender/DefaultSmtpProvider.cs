﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Helper.EmailSender
{
    public class DefaultSmtpProvider : SmtpBase, ISmtpProvider
    {
 

        /// <summary>
        /// 
        /// </summary>
        /// <param name="host"></param>
        /// <param name="port"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="enableSsl"></param>
        public DefaultSmtpProvider(string host, int port, string username, string password, bool enableSsl) : base( host,  port,  username,  password,  enableSsl)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool SendEmail(string toEmailAddress ,string fromEmailAddress, string subject, string body)
        {
            return base.Send(toEmailAddress, fromEmailAddress, subject, body);             
        }
    }
}
