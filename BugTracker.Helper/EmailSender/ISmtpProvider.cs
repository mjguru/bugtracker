﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Helper.EmailSender
{
    public interface ISmtpProvider
    {
        string ErrorMessage { get; set; }

        bool SendEmail(string toEmailAddress, string fromEmailAddress, string subject, string body);
    }
}
