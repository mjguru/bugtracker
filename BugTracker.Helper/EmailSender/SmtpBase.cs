﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Helper.EmailSender
{
    public class SmtpBase
    {
        private string _host { get; set; }

        private int _port { get; set; }

        private string _username { get; set; }

        private string _password { get; set; }

        private bool _enableSsl { get; set; }

        public string ErrorMessage { get; set; }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="host"></param>
        /// <param name="port"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="enableSsl"></param>
        protected SmtpBase(string host, int port, string username, string password, bool enableSsl)
        {
            _host = host;
            _port = port;
            _username = username;
            _password = password;
            _enableSsl = enableSsl;
            ErrorMessage = "";

        }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="toEmailAddress"></param>
      /// <param name="fromEmailAddress"></param>
      /// <param name="subject"></param>
      /// <param name="body"></param>
      /// <returns></returns>
        protected bool Send(string toEmailAddress, string fromEmailAddress, string subject, string body)
        {
            bool success = false;

            try
            {
                SmtpClient smtpClient = new SmtpClient(_host, _port);
                smtpClient.UseDefaultCredentials = false;

                smtpClient.Credentials = new System.Net.NetworkCredential(_username, _password);
                
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.EnableSsl = _enableSsl;


                MailMessage mail = new MailMessage();

                //Setting From , To and CC
                mail.From = new MailAddress(fromEmailAddress);
                mail.To.Add(new MailAddress(toEmailAddress));
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;
                

                smtpClient.Send(mail);

                success = true;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                ErrorMessage = ex.Message;

            }

            return success;


        }
    }
}
