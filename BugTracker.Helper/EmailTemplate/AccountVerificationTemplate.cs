﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Helper.EmailTemplate
{
    public class AccountVerificationTemplate
    {
        public static string GetTemplate()
        {
            string template = "";
            string baseUrl = ConfigurationManager.AppSettings["AccountVerificationLinkUrl"].Trim() + "verifyaccount/{{identifier}}";

            template = "Dear {{FullName}}<br><br> " + 

            "Welcome to JService Desk. To activate your account and to verify your e-mail address, please click the on the following link: " +
            "<br><br> " +

            "<a href = '" + baseUrl + "' target = '_blank'>" + baseUrl + "</a> " + 

            "<br><br> " +
            "Thank you for using JService Desk. " +

            "<br><br> " +
            "This is a post only mailing. Replies to this message are not monitored or answered.";  

            return template;
        }
    }
}
