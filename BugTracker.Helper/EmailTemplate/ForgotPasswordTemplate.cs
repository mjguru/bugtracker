﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Helper.EmailTemplate
{
    public class ForgotPasswordTemplate
    {
        public static string GetTemplate()
        {
            string template = "";
            string baseUrl = ConfigurationManager.AppSettings["ForgotPasswordLinkUrl"].Trim() + "resetpassword/{{identifier}}";

            template = "Dear {{FullName}}<br><br> " +

            "We were told that you forgot your password for JService Desk. To reset your password, please click the on the following link: " +
            "<br><br> " +

            "<a href = '" + baseUrl + "' target = '_blank'>" + baseUrl + "</a> " +

             "<br><br> " +
            "<h3>Please note that this link is valid for the next 5 minutes only.</h3>" +

            "<br><br> " +
            "Thank you for using JService Desk. " +

            "<br><br> " +
            "This is a post only mailing. Replies to this message are not monitored or answered.";

            return template;
        }
    }
}
