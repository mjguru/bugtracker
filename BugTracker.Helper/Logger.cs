﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Helper
{
    public class Logger
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="errorMessage"></param>
        public static void LogError(string errorMessage)
        {             
            ErrorLogger.LogManager.LogError(errorMessage);
        }

        public static void LogInformation(string errorMessage)
        {
            ErrorLogger.LogManager.LogInformation(errorMessage);
        }
    }
}
