﻿using BugTracker.Helper.EmailSender;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Helper
{
    public class MailSender
    {
       
        ISmtpProvider sender;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="host"></param>
        /// <param name="port"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="enableSsl"></param>
        public MailSender(string host, int port, string username, string password, bool enableSsl)
        {
            sender = new DefaultSmtpProvider(host, port, username, password, enableSsl);
        }

        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toEmailAddress"></param>
        /// <param name="fromEmailAddress"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        public MailSenderResponse Send(string toEmailAddress, string fromEmailAddress, string subject, string body)
        {
            MailSenderResponse response = new MailSenderResponse();

            response.Success = sender.SendEmail(toEmailAddress, fromEmailAddress, subject, body);
            response.ErrorMessage = sender.ErrorMessage;

            return response;
        }
    }

    public class MailSenderResponse
    {
        public bool Success { get; set; } = false;

        public string ErrorMessage { get; set; } = "";
    }

}
