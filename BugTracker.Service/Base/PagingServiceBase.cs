﻿using AutoMapper;
using BugTracker.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Service.Base
{
    public abstract class PagingServiceBase
    {
        public void SetPagingResult<Response, EntityModel, ServiceModel>(Response response, IQueryable<EntityModel> query, int pageSize, int currentPageNo) 
                                where Response : PagingBaseResponse<ServiceModel>
        {
            response.TotalRecords = query.Count(); 

            var dbItems = query.Skip(pageSize * (currentPageNo - 1)).Take(pageSize).ToList();

            response.Items = Mapper.Map<List<EntityModel>, List<ServiceModel>>(dbItems);
            response.CurrentPageNo = currentPageNo;

        }
    }
}
