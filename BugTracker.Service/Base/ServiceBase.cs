﻿using BugTracker.Helper;
using BugTracker.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace BugTracker.Service.Base
{
    public abstract class ServiceBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TResponse"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="action"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public TResponse RunMethod<TResponse, T>(Func<T, TResponse> action, T b) where TResponse : BaseResponse, new()
        {
            TResponse result = new TResponse();

            try
            {
                result = action(b);
            }
            catch (Exception ex)
            {
                result.IsSuccessful = false;

                string error = ex.Message + " Inner Exception: " +  ex.InnerException;
                result.ErrorMessage = error;

                Logger.LogError(error);
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TResponse"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="action"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public TResponse RunMethodWithTransaction<TResponse, T>(Func<T, TResponse> action, T b) where TResponse : BaseResponse, new()
        {
            TResponse result = new TResponse();

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    result = action(b);
                    scope.Complete();

                }
            }
            catch (Exception ex)
            {
                result.IsSuccessful = false;

                string error = ex.Message + " Inner Exception: " + ex.InnerException;
                result.ErrorMessage = error;

                Logger.LogError(error);
            }

            return result;
        }
    }
}
