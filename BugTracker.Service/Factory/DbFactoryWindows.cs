﻿using BugTracker.Data.Repository.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Service.Factory
{
    public class DbFactoryWindows : Disposable
    {
        public DbFactory dbFactory { get; private set; }
        
        /// <summary>
        /// 
        /// </summary>
        public DbFactoryWindows()
        {
            dbFactory = new DbFactory();
        }

        protected override void DisposeCustomObject()
        {
            if (dbFactory != null)
                dbFactory.Dispose();
        }

    }
}
