﻿using BugTracker.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Service.Factory
{
    public interface IPortalServiceBase : IUserRegistrationService, ICompanyService, IUserService, IForgotPasswordTokenService, IClientService
    {
    }
}
