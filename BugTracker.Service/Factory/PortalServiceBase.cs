﻿using BugTracker.Data.Repository.Infrastructure;
using BugTracker.Data.Repository.Repositories.Interface;
using BugTracker.Service.Base;
using BugTracker.Service.Interface;
using BugTracker.Service.Mapping;
using BugTracker.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 

namespace BugTracker.Service.Factory
{
    public class PortalServiceBase : ServiceBase, IPortalServiceBase
    {
        private readonly IUserRegistrationService _userRegistrationService;
        private readonly ICompanyService _companyService;
        private readonly IUserService _userService;
        private readonly IForgotPasswordTokenService _forgotPasswordTokenService;

        private readonly IClientService _clientService;

        public PortalServiceBase(IUserService userService, IUserRegistrationService userRegistrationService, ICompanyService companyService, 
            IForgotPasswordTokenService forgotPasswordTokenService, IClientService clientService)
        {
            _userRegistrationService = userRegistrationService;
            _companyService = companyService;
            _userService = userService;
            _forgotPasswordTokenService = forgotPasswordTokenService;
            _clientService = clientService;
        }


        public ClientResponse GetClientsByCompany(ClientRequest request)
        {
            return RunMethod(_clientService.GetClientsByCompany, request);
        }

        public UserResponse ChangePassword(UserRequest request)
        {
            return RunMethod(_userService.ChangePassword, request);
        }

        public UserResponse ChangePasswordViaForgotToken(UserRequest request)
        {
            return RunMethod(_userService.ChangePasswordViaForgotToken, request);
        }

        public ForgotPasswordTokenResponse CheckForgotPasswordToken(ForgotPasswordTokenRequest request)
        {
            return RunMethodWithTransaction(_forgotPasswordTokenService.CheckForgotPasswordToken, request);
        }

        public UserResponse CreateNewUser(UserRequest request)
        {            
            return RunMethodWithTransaction(_userRegistrationService.CreateNewUser, request);
        }

        public ForgotPasswordTokenResponse ExpireForgotPasswordToken(ForgotPasswordTokenRequest request)
        {
            return RunMethodWithTransaction(_forgotPasswordTokenService.ExpireForgotPasswordToken, request);
        }

        public ForgotPasswordTokenResponse GenerateForgotPasswordToken(ForgotPasswordTokenRequest request)
        {
            return RunMethodWithTransaction(_forgotPasswordTokenService.GenerateForgotPasswordToken, request);
        }

        

        public CompanyResponse GetCompanyByName(CompanyRequest request)
        {
            return RunMethod(_companyService.GetCompanyByName, request);
        }

        public UserResponse GetUserByEmail(UserRequest request)
        {
            return RunMethod(_userService.GetUserByEmail, request);
        }

        public UserResponse GetUserByIdentifier(UserRequest request)
        {
            return RunMethod(_userService.GetUserByIdentifier, request);
        }

        public UserResponse GetUsersForActivationEmail(UserRequest request)
        {
            return RunMethod(_userService.GetUserByEmail, request);
        }

        public UserResponse UpdateBasicDetail(UserRequest request)
        {
            return RunMethodWithTransaction(_userRegistrationService.UpdateBasicDetail, request);
        }

        public UserResponse ValidateLogin(UserRequest request)
        {
            return RunMethod(_userService.ValidateLogin, request);
        }

        public UserResponse VerifyUserAccount(UserRequest request)
        {
            return RunMethod(_userService.VerifyUserAccount, request);
        }
    }
}
