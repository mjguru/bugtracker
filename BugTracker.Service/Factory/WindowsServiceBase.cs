﻿using BugTracker.Data.Repository.Infrastructure;
using BugTracker.Data.Repository.Repositories.Implementation;
using BugTracker.Data.Repository.Repositories.Interface;
using BugTracker.Service.Base;
using BugTracker.Service.Implementation;
using BugTracker.Service.Interface;
using BugTracker.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Service.Factory
{
    public class WindowsServiceBase : ServiceBase, IWindowsServiceBase
    {
       
        public WindowsServiceBase()
        {
            
        }

        /// <summary>
        /// Had to create this method because during parallel thread execution DbContext is being shared and throwing error, 
        /// to avoid the scenario we need to create a context for each request
        /// </summary>
        /// <param name="dbFactory"></param>
        /// <param name="uow"></param>
        /// <returns></returns>
        private IWindowsService GetWindowsServiceReference()
        {
            var windowsFactory = new DbFactoryWindows();

            IDbFactory dbFactory = windowsFactory.dbFactory;
            var uow = new UnitOfWork(dbFactory);

            return new WindowsService(new UserRepository(dbFactory), uow, new EmailRepository(dbFactory));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public UserResponse SendActivationEmail(UserRequest request)
        {
            return RunMethodWithTransaction(GetWindowsServiceReference().SendActivationEmail, request);           
        }

        public EmailResponse GetPendingEmailToSend(EmailRequest request)
        {
            return RunMethodWithTransaction(GetWindowsServiceReference().GetPendingEmailToSend, request);
        }

        public EmailResponse UpdateSentEmailStatus(EmailRequest request)
        {
            return RunMethodWithTransaction(GetWindowsServiceReference().UpdateSentEmailStatus, request);
        }
    }
}
