﻿using BugTracker.Data.Repository.Infrastructure;
using BugTracker.Data.Repository.Repositories.Interface;
using db = BugTracker.Database.Model;
using BugTracker.Service.Interface;
using BugTracker.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BugTracker.Constants.Enum;
using AutoMapper;
using BugTracker.Database.Model;
using BugTracker.Service.Base;

namespace BugTracker.Service.Implementation
{
    public class ClientService : PagingServiceBase, IClientService
    {
        private readonly IClientRepository _clientRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ClientService(IClientRepository clientRepository, IUnitOfWork unitOfWork)
        {
            _clientRepository = clientRepository;
            _unitOfWork = unitOfWork;
        }

        public ClientResponse GetClientsByCompany(ClientRequest request)
        {
            var response = new ClientResponse();
             

            var query = _clientRepository.GetClientQueryByCompany(request.CompanyIdentifier).OrderBy(o => o.Id);

            SetPagingResult<ClientResponse, Client, ClientSVM>(response,query, request.PageSize, request.CurrentPageNo );

            

            return response;
        }
    }
}
