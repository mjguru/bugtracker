﻿using BugTracker.Data.Repository.Infrastructure;
using BugTracker.Data.Repository.Repositories.Interface;
using db = BugTracker.Database.Model;
using BugTracker.Service.Interface;
using BugTracker.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BugTracker.Constants.Enum;

namespace BugTracker.Service.Implementation
{
    public class CompanyService : ICompanyService
    {
        private readonly ICompanyRepository _companyRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CompanyService(ICompanyRepository companyRepository, IUnitOfWork unitOfWork)
        {
            _companyRepository = companyRepository;
            _unitOfWork = unitOfWork;
        }        


        public CompanyResponse GetCompanyByName(CompanyRequest request)
        {
            CompanyResponse response = new CompanyResponse();

            var user = _companyRepository.GetCompanyByName(request.Name.Trim());

            response.Exists = (user != null);

            return response;
        }

        
    }
}
