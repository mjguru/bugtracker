﻿using BugTracker.Data.Repository.Infrastructure;
using BugTracker.Data.Repository.Repositories.Interface;
using db = BugTracker.Database.Model;
using BugTracker.Service.Interface;
using BugTracker.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BugTracker.Constants.Enum;
using BugTracker.Database.Model;
using BugTracker.Helper.EmailTemplate;

namespace BugTracker.Service.Implementation
{
    public class ForgotPasswordTokenService : IForgotPasswordTokenService
    {
        private readonly IForgotPasswordTokenRepository _forgotPasswordTokenRepository;
        private readonly IEmailRepository _emailRepository;
        private readonly IUserRepository _userRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ForgotPasswordTokenService(IForgotPasswordTokenRepository forgotPasswordTokenRepository, IUnitOfWork unitOfWork, IUserRepository userRepository, IEmailRepository emailRepository)
        {
            _forgotPasswordTokenRepository = forgotPasswordTokenRepository;
            _unitOfWork = unitOfWork;
            _userRepository = userRepository;
            _emailRepository = emailRepository;
        }

        public ForgotPasswordTokenResponse CheckForgotPasswordToken(ForgotPasswordTokenRequest request)
        {
            ForgotPasswordTokenResponse response = new ForgotPasswordTokenResponse();

            var forgotPasswordToken = _forgotPasswordTokenRepository.GetValidTokenByIdentifier(request.Indentifier);

            response.Exists = (forgotPasswordToken != null);

            if (forgotPasswordToken != null)
            {
                response.UserIdentifier = forgotPasswordToken.User.Identifier;
            }

            return response;
        }

        public ForgotPasswordTokenResponse ExpireForgotPasswordToken(ForgotPasswordTokenRequest request)
        {
            ForgotPasswordTokenResponse response = new ForgotPasswordTokenResponse();
             

            var forgotPasswordTokenList = _forgotPasswordTokenRepository.GetValidTokenByUser(request.UserIndentifier);

            foreach (var item in forgotPasswordTokenList)
            {
                item.IsActive = false;
                _EntityBaseProperty.Set(item);
            }

            SaveForgotPasswordToken();

            return response;
        }

        public ForgotPasswordTokenResponse GenerateForgotPasswordToken(ForgotPasswordTokenRequest request)
        {
            ForgotPasswordTokenResponse response = new ForgotPasswordTokenResponse();

            var user = _userRepository.GetVerifiedUserByEmail(request.EmailAddress);

            if (user != null)
            {
                ForgotPasswordToken token = new ForgotPasswordToken();
                _EntityBaseProperty.Set(token);

                token.UserFK = user.Id;
                token.ValidUntil = DateTime.Now.AddMinutes(5);
                _forgotPasswordTokenRepository.Add(token);

                SaveForgotPasswordToken();

                //now send email
                CreateEmailForPasswordForgotLink(token.Identifier, user);

            }
            else
            {
                response.Exists = false;
            }

            return response;
        }

        private void CreateEmailForPasswordForgotLink(Guid identifier, db.User user)
        { 
                //send email
                db.Email email = new db.Email();

                email.ToAddress = user.EmailAddress;

                string body = ForgotPasswordTemplate.GetTemplate();
                body = body.Replace("{{FullName}}", user.Firstname + " " + user.Lastname);
                body = body.Replace("{{identifier}}", identifier.ToString());
                email.Body = body;


                email.IsSent = false;
                email.UserFK = user.Id;
                email.Subject = "JService Desk - Forgot Password Request";
             

                _EntityBaseProperty.Set(email);

                _emailRepository.Add(email);

            //finally save
            SaveForgotPasswordToken();
        }

        /// <summary>
        /// 
        /// </summary>
        private void SaveForgotPasswordToken()
        {
            _unitOfWork.Commit();
        }
    }
}
