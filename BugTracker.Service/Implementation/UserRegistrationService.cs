﻿using BugTracker.Data.Repository.Infrastructure;
using BugTracker.Data.Repository.Repositories.Interface;
using db = BugTracker.Database.Model;
using BugTracker.Service.Interface;
using BugTracker.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BugTracker.Constants.Enum;
using System.Transactions;

namespace BugTracker.Service.Implementation
{
    public class UserRegistrationService : IUserRegistrationService
    {
        private readonly ICompanyRepository _companyRepository;
        private readonly IUserRepository _userRepository;
        private readonly IUnitOfWork _unitOfWork;

        public UserRegistrationService(ICompanyRepository companyRepository, IUserRepository userRepository, IUnitOfWork unitOfWork)
        {
            _companyRepository = companyRepository;
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
        }
           
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public UserResponse CreateNewUser(UserRequest request)
        { 
            UserResponse response = new UserResponse();
            
                //first create company
                var company = new db.Company() { Name = request.CompanyName.Trim() };
                _EntityBaseProperty.Set(company);

                _companyRepository.Add(company);
                SaveChanges();


            //now create user
            var user = request.User;
                user.CompanyFK = company.Id;

                response = AddNewUser(user);

             
            return response;
        }

        private UserResponse AddNewUser(UserSVM user)
        {
            UserResponse response = new UserResponse();

            db.User userToCreate = new db.User()
            {
                Identifier = Guid.NewGuid(),
                ActivationLinkIdentifier = Guid.NewGuid(),
                IsVerified = false,
                CompanyFK = user.CompanyFK,
                RoleFK = (int)Role.CompanyAdmin,
                UserTypeFK = (int)UserType.Company,
                Firstname = user.Firstname.Trim(),
                Lastname = user.Lastname.Trim(),
                EmailAddress = user.EmailAddress.Trim(),
                Password = user.Password.Trim(),
                Salt = user.Salt.Trim(),
                NoOfIncorrectLoginAttempts = 0,
                IsLock = false,
                IsActive = true,
                IsActivationEmailSent = false
            };

            _EntityBaseProperty.Set(userToCreate);

            //save
            _userRepository.Add(userToCreate);
            SaveChanges();

            return response;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public UserResponse UpdateBasicDetail(UserRequest request)
        {
            UserResponse response = new UserResponse();

            var user = _userRepository.GetUserByIdentifier(request.User.Identifier);
            if (user != null)
            {

                //first check if company exists
                var existingCompany = _companyRepository.GetCompanyByName(request.CompanyName.Trim());
                if (existingCompany != null && existingCompany.Id != user.CompanyFK)
                {
                    response.CompanyExists = true;
                }
                else
                {
                    var company = _companyRepository.GetById(user.CompanyFK.Value);
                    if (company != null)
                    {
                        company.Name = request.CompanyName.Trim();
                        _EntityBaseProperty.Set(company);
                        _companyRepository.Update(company);

                        user.Firstname = request.User.Firstname.Trim();
                        user.Lastname = request.User.Lastname.Trim();

                        _EntityBaseProperty.Set(user);
                        _userRepository.Update(user);

                        SaveChanges();
                    }
                    else
                    {
                        response.IsSuccessful = false;
                    } 
                } 
                             
            }
            else
            {
                response.IsSuccessful = false;
            }

            return response;
        }

        private void SaveChanges()
        {
            _unitOfWork.Commit();
        }

    }
}
