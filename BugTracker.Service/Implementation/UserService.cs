﻿using BugTracker.Data.Repository.Infrastructure;
using BugTracker.Data.Repository.Repositories.Interface;
using db = BugTracker.Database.Model;
using BugTracker.Service.Interface;
using BugTracker.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BugTracker.Constants.Enum;
using AutoMapper;
using BugTracker.Database.Model;

namespace BugTracker.Service.Implementation
{
    public class UserService : IUserService
    {
        protected readonly IUserRepository _userRepository;
        protected readonly IUnitOfWork _unitOfWork;

        public UserService(IUserRepository userRepository, IUnitOfWork unitOfWork)
        {
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
        }

        public UserResponse ChangePasswordViaForgotToken(UserRequest request)
        {
            UserResponse response = new UserResponse();

            var user = _userRepository.GetUserByIdentifier(request.User.Identifier);

            if (user != null)
            {
                user.Password = request.User.Password.Trim();
                user.Salt = request.User.Salt.Trim();
                user.NoOfIncorrectLoginAttempts = 0;
                user.LastPasswordResetDate = DateTime.Now;

                _EntityBaseProperty.Set(user);
                _userRepository.Update(user);
                SaveUser();
            }
            else
            {
                response.IsSuccessful = false;
            }

            return response;
        }

        public UserResponse GetUserByEmail(UserRequest request)
        {
            UserResponse response = new UserResponse();

            var user = _userRepository.GetUserByEmail(request.User.EmailAddress.Trim());

            response.Exists = (user != null);

            return response;
        }

     
        public UserResponse VerifyUserAccount(UserRequest request)
        {
            UserResponse response = new UserResponse();

            var user = _userRepository.GetUserByUnverfiedActivationLink(request.ActivationLink);

            if (user != null)
            {
                user.IsVerified = true;
                user.VerifiedOn = DateTime.Now;
                SaveUser();
            }

            response.Exists = (user != null);

            return response;
        }

        private bool IsPasswordValidForUser(string password, User user)
        {
            return (user.Password == Helper.PasswordEncryptor.GenerateHMAC(password, user.Salt));
        }

        public UserResponse ValidateLogin(UserRequest request)
        {
            UserResponse response = new UserResponse();

            var user = _userRepository.GetVerifiedUserByEmail(request.User.EmailAddress);

            if (user != null)
            {
                if (user.AccountLockedUntil >= DateTime.Now)
                {
                    response.IsAccountLocked = true;
                }
                else
                {
                    bool isAuthenticated = IsPasswordValidForUser(request.User.Password, user);

                    if (isAuthenticated)
                    {
                        user.NoOfIncorrectLoginAttempts = 0;
                        user.IsLock = false;
                        response.IsValidLogin = true;
                        user.AccountLockedUntil = null;

                        //set response
                        response.EmailAddress = user.EmailAddress;
                        response.Firstname = user.Firstname;
                        response.Lastname = user.Lastname;
                        response.Identifier = user.Identifier;
                        response.Role = user.lkRole.Name;
                        response.CompanyName = user.Company.Name;
                        response.CompanyIdentifier = user.Company.Identifier;

                    }
                    else
                    {
                        int noOfIncorrectLoginAttempts = user.NoOfIncorrectLoginAttempts;

                        user.NoOfIncorrectLoginAttempts = noOfIncorrectLoginAttempts + 1;

                        if (user.NoOfIncorrectLoginAttempts >= 3)
                        {
                            user.IsLock = true;
                            user.AccountLockedUntil = DateTime.Now.AddMinutes(15);
                        }
                        

                        response.IsValidLogin = false;
                    }

                    user.LastActivityDate = DateTime.Now;
                    _EntityBaseProperty.Set(user);
                    _userRepository.Update(user);
                    SaveUser();
                }
                
            }
            else
            {
                response.IsValidLogin = false;
            }

            return response;
        }

        public UserResponse GetUserByIdentifier(UserRequest request)
        {
            UserResponse response = new UserResponse();

            var user = _userRepository.GetUserByIdentifier(request.User.Identifier);
            if (user != null)
            {
                response.User = Mapper.Map<User, UserSVM>(user);
            }
            

            return response;
        }


        public UserResponse ChangePassword(UserRequest request)
        {
            UserResponse response = new UserResponse();

            var user = _userRepository.GetUserByIdentifier(request.User.Identifier);
            if (user == null)
            {
                response.IsSuccessful = false;
            }
            else
            {
                bool isAuthenticated = IsPasswordValidForUser(request.User.CurrentPassword, user);
                if (!isAuthenticated)
                {
                    response.OldPasswordNotMatch = true;
                }
                else
                {
                    user.Salt = request.User.Salt.Trim();
                    user.Password = request.User.Password.Trim();

                    _EntityBaseProperty.Set(user);

                    //save
                    _userRepository.Update(user);

                    SaveUser();
                }
            }


            return response;
        }

        private void SaveUser()
        {
            _unitOfWork.Commit();
        }

       
    }
}
