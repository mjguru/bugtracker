﻿using BugTracker.Data.Repository.Infrastructure;
using BugTracker.Data.Repository.Repositories.Interface;
using db = BugTracker.Database.Model;
using BugTracker.Service.Interface;
using BugTracker.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BugTracker.Constants.Enum;
using BugTracker.Helper.EmailTemplate;

namespace BugTracker.Service.Implementation
{
    public class WindowsService : IWindowsService
    {
        protected readonly IUserRepository _userRepository;
        protected readonly IUnitOfWork _unitOfWork;
        private readonly IEmailRepository _emailRepository;


        public WindowsService(IUserRepository userRepository, IUnitOfWork unitOfWork, IEmailRepository emailRepository)
        {
            _userRepository = userRepository;
            _emailRepository = emailRepository;
            _unitOfWork = unitOfWork;
        }

       


        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public UserResponse SendActivationEmail(UserRequest request)
        {
            UserResponse response = new UserResponse();

            var userList = _userRepository.GetUsersWithActivationEmailToSend();

            foreach (var user in userList)
            {
                //send email
                db.Email email = new db.Email();

                email.Identifier = Guid.NewGuid();
                email.ToAddress = user.EmailAddress;

                string body = AccountVerificationTemplate.GetTemplate();
                body = body.Replace("{{FullName}}", user.Firstname + " " + user.Lastname);
                body = body.Replace("{{identifier}}", user.ActivationLinkIdentifier.Value.ToString());
                email.Body = body;


                email.IsSent = false;
                email.UserFK = user.Id;

                email.Subject = "JService Desk - Please verify your email address";

                 
                email.IsActive = true;

                _EntityBaseProperty.Set(email);

                _emailRepository.Add(email);


                //updated user
                user.IsActivationEmailSent = true;
                user.ActivationEmailSentDate = DateTime.Now;

                _EntityBaseProperty.Set(user);

                _userRepository.Update(user);

                //finally save
                Save();

            }


            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public EmailResponse GetPendingEmailToSend(EmailRequest request)
        {
            EmailResponse response = new EmailResponse();

            var list = _emailRepository.GetPendingEmailToSend();
            if (list != null && list.Count > 0)
            {
                response.Emails = (from e in list
                                   select new EmailSVM()
                                   {
                                       Id = e.Id,
                                       Body = e.Body,
                                       Subject = e.Subject,
                                       ToAddress = e.ToAddress
                                   }
                    ).ToList();
            }


            return response;
        }

        public EmailResponse UpdateSentEmailStatus(EmailRequest request)
        {
            EmailResponse response = new EmailResponse();

            var email = _emailRepository.GetById(request.Id);
            if (email != null)
            {
                email.Id = request.Id;
                email.IsSent = request.IsSent;
                email.ErrorMessage = request.ErrorMessage;

                if (request.IsSent)
                {
                    email.SentDate = request.SentDate;
                }

                _EntityBaseProperty.Set(email);

                _emailRepository.Update(email);
                Save();

            }


            return response;
        }

        private void Save()
        {
            _unitOfWork.Commit();
        }


    }
}
