﻿using BugTracker.Database.Model;
using BugTracker.Helper.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Service.Implementation
{
    public class _EntityBaseProperty
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        public static void Set<T>(T obj) where T : EntityBase
        {
            var authContext = new AuthContext();

            if (obj.Id > 0)
            {
                obj.UpdatedBy = authContext.UserIdentifier.ToString();
                obj.UpdatedOn = DateTime.Now;
            }
            else
            {
                obj.IsActive = true;
                obj.Identifier = Guid.NewGuid();
                obj.CreatedOn = DateTime.Now;
                obj.CreatedBy = authContext.UserIdentifier.ToString();
            }
        }
    }
}
