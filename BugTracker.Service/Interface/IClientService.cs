﻿using BugTracker.Database.Model;
using BugTracker.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Service.Interface
{
    public interface IClientService
    {
        ClientResponse GetClientsByCompany(ClientRequest request);

    }
}
