﻿using BugTracker.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Service.Interface
{
    public interface IUserRegistrationService 
    {
        UserResponse CreateNewUser(UserRequest request);

        UserResponse UpdateBasicDetail(UserRequest request);
    }
}
