﻿using BugTracker.Database.Model;
using BugTracker.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Service.Interface
{
    public interface IUserService
    {
        UserResponse GetUserByEmail(UserRequest request);

        UserResponse VerifyUserAccount(UserRequest request);

        UserResponse ChangePasswordViaForgotToken(UserRequest request);

        UserResponse ValidateLogin(UserRequest request);

        UserResponse GetUserByIdentifier(UserRequest request);

        UserResponse ChangePassword(UserRequest request);


    }
}
