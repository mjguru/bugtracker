﻿using AutoMapper;
using BugTracker.Database.Model;
using BugTracker.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Service.Mapping
{
    public class EntityToViewModelMappingProfileServiceLayer : Profile
    {
        public override string ProfileName
        {
            get { return "EntityToViewModelMappingServiceLayer"; }
        }

        public EntityToViewModelMappingProfileServiceLayer()
        {
             CreateMap<User, UserSVM>()
                .ForMember(g => g.CompanyName, map => map.MapFrom(v => v.Company.Name))
                .ForMember(g => g.Password, map => map.Ignore())
                .ForMember(g => g.Salt, map => map.Ignore())
                .ForMember(g => g.CompanyFK, map => map.Ignore())
                ;


            CreateMap<Client, ClientSVM>();


        }

    }
}
