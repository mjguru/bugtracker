﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Service.Model
{
    public class BaseRequest
    {
        public int PageSize { get; set; } = 10;

        public int CurrentPageNo { get; set; } = 0;
    }
}
