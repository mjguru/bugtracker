﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Service.Model
{
    public class BaseResponse
    {
        public bool IsSuccessful { get; set; } = true;

        public string ErrorMessage { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isSuccess"></param>
        /// <param name="errorMessage"></param>
        protected void OnResponse(bool isSuccess, string errorMessage)
        {
            IsSuccessful = isSuccess;
            if (!isSuccess)
            {
                ErrorMessage = errorMessage;
            }

        }
    }
}
