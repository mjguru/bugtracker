﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Service.Model
{
    public class ClientSVM
    {
        public Guid Identifier { get; set; }

        public string Address { get; set; }

        public string Name { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
