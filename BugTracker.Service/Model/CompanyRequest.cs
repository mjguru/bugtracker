﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Service.Model
{
    public class CompanyRequest : BaseRequest
    {
        public string Name { get; set; }

    }
}
