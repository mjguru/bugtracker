﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Service.Model
{
    public class CompanyResponse : BaseResponse
    {
        public bool Exists { get; set; }

        public int Id { get; set; }
    }
}
