﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Service.Model
{
    public class EmailRequest : BaseRequest
    {
        public int Id { get; set; }

        public string ErrorMessage { get; set; }

        public bool IsSent { get; set; }

        public DateTime SentDate { get; set; }

    }
}
