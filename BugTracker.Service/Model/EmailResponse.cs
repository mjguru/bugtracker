﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Service.Model
{
    public class EmailResponse : BaseResponse
    {
        public List<EmailSVM> Emails { get; set; }
    }
}
