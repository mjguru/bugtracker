﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Service.Model
{
    public class ForgotPasswordTokenRequest : BaseRequest
    {
        public string EmailAddress { get; set; }

        public Guid Indentifier { get; set; }

        public Guid UserIndentifier { get; set; }

    }
}
