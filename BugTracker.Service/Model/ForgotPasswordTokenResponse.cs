﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Service.Model
{
    public class ForgotPasswordTokenResponse : BaseResponse
    {

        public bool Exists { get; set; } = true;

        public Guid UserIdentifier { get; set; }
    }
}
