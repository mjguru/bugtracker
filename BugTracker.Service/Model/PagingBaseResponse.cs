﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Service.Model
{
    public class PagingBaseResponse<T> : BaseResponse
    {
        public List<T> Items { get; set; }

        public int TotalRecords { get; set; }

        public int CurrentPageNo { get; set; }
    }
}
