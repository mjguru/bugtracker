﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Service.Model
{
    public class UserRequest : BaseRequest
    {
        public UserSVM User { get; set; }

        public string CompanyName { get; set; }

        public Guid ActivationLink { get; set; }

   
    }
}
