﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Service.Model
{
    public class UserResponse : BaseResponse
    {
        public bool Exists { get; set; }

        public UserSVM User { get; set; }

        public List<UserSVM> Users { get; set; }

        public bool IsAccountLocked { get; set; } 

        public bool IsValidLogin { get; set; } = false;

        public string EmailAddress { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public Guid Identifier { get; set; }

        public string Role { get; set; }

        public bool CompanyExists { get; set; }

        public bool OldPasswordNotMatch { get; set; }

        public Guid CompanyIdentifier { get; set; }
        public string CompanyName { get; set; }



    }
}
