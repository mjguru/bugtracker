﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Service.Model
{
    public class UserSVM
    {
        public string CompanyName { get; set; }

        public Guid Identifier { get; set; }

        public string EmailAddress { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Password { get; set; }

        public string CurrentPassword { get; set; }

        

        public string Salt { get; set; }

        public int CompanyFK { get; set; }

    }
}
