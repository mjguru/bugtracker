﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace BugTracker.TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Expression< Action<int>> ShowIntAsString = (x) => Console.Write(x);
            
            testmethod();
            Console.ReadLine();
        }

        private static void testmethod()
        {
            List<int> numbers = new List<int> { 11, 37, 52 };
            var o = numbers.Find(x => x == 1);

            Func<int, string> test = (x) => showme(x);
            Console.WriteLine(test(252));
        }

        private static string showme(int number)
        {
            return number.ToString() +  " is my shirt no";
        }


        private static void CurrentItem(int currentNo)
        {
            Console.WriteLine("Processing Item no: " + currentNo);
        }

        private delegate void CallbackDelegate(int i);

        private static void LoopAll(CallbackDelegate func)
        {
            for (int i = 0; i < 100; i++)
            {
                func(i);
                for (int j = 0; j < int.MaxValue; j++)
                {

                }
            }
        }
    }
}
