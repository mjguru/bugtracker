﻿using BugTracker.WindowsService.Job;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BugTracker.WindowsService.Console
{
    class Program
    {
        static void Main(string[] args)
        { 

            // Common.Logging.LogManager.Adapter = new Common.Logging.Simple.ConsoleOutLoggerFactoryAdapter { Level = Common.Logging.LogLevel.Info };

            new JobConfiguration().RegisterJobs(); 

            System.Console.ReadLine();


        }

       
    }
}
