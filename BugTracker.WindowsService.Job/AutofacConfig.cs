﻿using Autofac;
using BugTracker.Data.Repository.Infrastructure;
using BugTracker.Data.Repository.Repositories.Implementation;
using BugTracker.Service.Implementation;
using BugTracker.Service.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Autofac.Extras.Quartz;
using Quartz;
using System.Collections.Specialized;
using Quartz.Impl;
using System.Web.Mvc;
using Unity.Mvc5;
using Autofac.Integration.Mvc;
using BugTracker.Data.Repository.Repositories.Interface;
using BugTracker.Service.Interface;

namespace BugTracker.WindowsService.Job
{
    public class AutofacConfig
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static IContainer ConfigureDependency()
        {
            var builder = new ContainerBuilder();


            //builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();
            //builder.RegisterType<DbFactory>().As<IDbFactory>().InstancePerLifetimeScope();
            //builder.RegisterType<BugTracker.Service.Implementation.WindowsService>().As<IWindowsService>().InstancePerLifetimeScope();

            //builder.RegisterAssemblyModules(Assembly.GetExecutingAssembly());

            

            //builder.RegisterType<UserRepository>().As<IUserRepository>().InstancePerLifetimeScope();
            //builder.RegisterType<EmailRepository>().As<IEmailRepository>().InstancePerLifetimeScope();

            builder.RegisterType<WindowsServiceBase>().As<IWindowsServiceBase>().InstancePerLifetimeScope();
            




            // Schedule
            builder.Register(x => new StdSchedulerFactory().GetScheduler().Result).As<IScheduler>().InstancePerLifetimeScope();

            // Schedule jobs
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly()).Where(x => typeof(IJob).IsAssignableFrom(x)).InstancePerLifetimeScope();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            return container;
        } 
    }
}
