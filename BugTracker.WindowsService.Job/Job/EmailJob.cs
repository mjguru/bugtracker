﻿using BugTracker.Service.Factory;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using System.Configuration;
using BugTracker.Helper;

namespace BugTracker.WindowsService.Job
{
    [DisallowConcurrentExecution]
    public class EmailJob : IJob
    {
        private readonly IWindowsServiceBase ServiceFactory;

        public EmailJob()
        {

        }


        public EmailJob(IWindowsServiceBase _serviceFactory)
        {
            ServiceFactory = _serviceFactory;
        }

        public Task Execute(IJobExecutionContext context)
        {

            var response = ServiceFactory.GetPendingEmailToSend(new Service.Model.EmailRequest());

            if (response.IsSuccessful && (response.Emails != null && response.Emails.Count > 0))
            {
                var list = response.Emails;

               // send email
                foreach (var item in list)
                {
                    //Send email first
                    string errorMessage = "";
                    MailSenderResponse emailResponse = SendEmail(item.ToAddress, item.Subject, item.Body);
                    if (!emailResponse.Success)
                    {
                        errorMessage = emailResponse.ErrorMessage;
                    }

                    //update the database table
                    var updateResponse = ServiceFactory.UpdateSentEmailStatus(new Service.Model.EmailRequest()
                    {
                        Id = item.Id,
                        IsSent = emailResponse.Success,
                        SentDate = DateTime.Now,
                        ErrorMessage = errorMessage
                    });
                }

            }

            return Task.FromResult(0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toEmailAddress"></param>
        /// <param name="fromEmailAddress"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        private MailSenderResponse SendEmail(string toEmailAddress, string subject, string body)
        {
            string host = ConfigurationManager.AppSettings["smtp_host"];
            int port = Convert.ToInt32(ConfigurationManager.AppSettings["smtp_port"]);
            string username = ConfigurationManager.AppSettings["smtp_username"];
            string password = ConfigurationManager.AppSettings["smtp_password"];
            bool enableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["smtp_enableSsl"]);
            string fromEmailAddress = ConfigurationManager.AppSettings["smtp_from"];

            
            //Send Email
            MailSender sender = new MailSender(host, port, username, password, enableSsl);
            MailSenderResponse response = sender.Send(toEmailAddress, fromEmailAddress, subject, body);

            return response;

        }


    }
}
