﻿using BugTracker.Service.Factory;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;

namespace BugTracker.WindowsService.Job
{
    [DisallowConcurrentExecution]
    public class NewUserActivationEmailJob : IJob
    {
        private readonly IWindowsServiceBase ServiceFactory;

        public NewUserActivationEmailJob()
        {

        }


        public NewUserActivationEmailJob(IWindowsServiceBase _serviceFactory)
        {
            ServiceFactory = _serviceFactory;
        }

        public Task Execute(IJobExecutionContext context)
        {

            var response = ServiceFactory.SendActivationEmail(new Service.Model.UserRequest());
            if (response.IsSuccessful)
            {
                Console.WriteLine("NewUserActivationEmailJob job has set email");
            }

            return Task.FromResult(0);
        }


    }
}
