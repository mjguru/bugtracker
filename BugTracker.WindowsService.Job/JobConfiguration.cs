﻿using Autofac;
using Autofac.Extras.Quartz;
using Autofac.Integration.Mvc;
using BugTracker.Data.Repository.Infrastructure;
using BugTracker.Data.Repository.Repositories.Implementation;
using BugTracker.Data.Repository.Repositories.Interface;
using BugTracker.Service.Factory;
using BugTracker.Service.Implementation;
using BugTracker.Service.Interface;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Matchers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BugTracker.WindowsService.Job
{
    public class JobConfiguration
    {
        /// <summary>
        /// 
        /// </summary>
        public void RegisterJobs()
        {

            var container = AutofacConfig.ConfigureDependency();

            //Schedule
            IScheduler sched = container.Resolve<IScheduler>();
            sched.JobFactory = new BugTracker.WindowsService.Job.AutofacJobFactory(container);
            

            StackAndSetJobTrigger<NewUserActivationEmailJob>(Convert.ToInt32(ConfigurationManager.AppSettings["TestJobIntervalInSeconds"]), sched);
            StackAndSetJobTrigger<EmailJob>(Convert.ToInt32(ConfigurationManager.AppSettings["EmailJobIntervalInSeconds"]), sched);

            sched.Start();
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TJob"></typeparam>
        /// <param name="triggerTimeInSeconds"></param>
        /// <param name="sched"></param>
        private void StackAndSetJobTrigger<TJob>(int triggerTimeInSeconds, IScheduler sched) where TJob : IJob
        {
            var jobName = typeof(TJob).Name;
            string groupName = "Group_";// + typeof(TJob).Name + "_" + Guid.NewGuid().ToString();

            JobKey jobKey = new JobKey(jobName, groupName);

            //build job
            IJobDetail jobToBuild = JobBuilder.Create<TJob>()
                .WithIdentity(jobKey)
                .Build();

            //set job trigger
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity(Guid.NewGuid().ToString(), Guid.NewGuid().ToString())
              // .StartNow()
              

               .WithSimpleSchedule(x => x
               .WithIntervalInSeconds(triggerTimeInSeconds)
               .RepeatForever())

               .Build();

            //Listener attached 
            sched.ListenerManager.AddJobListener(
                new ScheduledJobListener(jobKey.Name), KeyMatcher<JobKey>.KeyEquals(jobKey)
            );

            //schedule the job
            sched.ScheduleJob(jobToBuild, trigger);
        }
    }
}
