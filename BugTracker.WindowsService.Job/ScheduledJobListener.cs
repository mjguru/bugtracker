﻿using BugTracker.Helper;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BugTracker.WindowsService.Job
{
    public class ScheduledJobListener : IJobListener
    {
        private string _jobName = "";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="jobName"></param>
        public ScheduledJobListener(string jobName)
        {            
            _jobName = jobName + "_listener";
        }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get { return _jobName; }  }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task JobExecutionVetoed(IJobExecutionContext context, CancellationToken cancellationToken = default(CancellationToken))
        {
            return Task.FromResult(0);
        }

        public Task JobToBeExecuted(IJobExecutionContext context, CancellationToken cancellationToken = default(CancellationToken))
        {
            string jobName = context.JobDetail.Key.Name;
            string message = "About to start Job " + jobName;

            LogSchedulerMessage(message);

            return Task.FromResult(0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="jobException"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task JobWasExecuted(IJobExecutionContext context, JobExecutionException jobException, CancellationToken cancellationToken = default(CancellationToken))
        {
            string jobName = context.JobDetail.Key.Name;
            string message = "===============Executed Job " + jobName + " =======================";

            LogSchedulerMessage(message);
 

            return Task.FromResult(0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        private void LogSchedulerMessage(string message)
        {
            Console.WriteLine(message);
            Logger.LogInformation(message);
            
            
        }
    }
}
