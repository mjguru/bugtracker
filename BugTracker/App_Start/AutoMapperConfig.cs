﻿using AutoMapper;
using BugTracker.Service.Mapping;
using BugTracker.Web.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BugTracker.Web.App_Start
{
    public class AutoMapperConfig
    {
        public static void Configure()
        { 

            Mapper.Initialize(x =>
            {
                x.AddProfile<EntityToViewModelMappingProfile>();
                x.AddProfile<ViewModelToEntityMappingProfile>();
                x.AddProfile<EntityToViewModelMappingProfileServiceLayer>();
            });
        }
    }
}