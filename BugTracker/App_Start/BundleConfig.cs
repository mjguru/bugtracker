﻿using System.Web;
using System.Web.Optimization;

namespace BugTracker.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/jquery.unobtrusive-ajax*",
                        "~/Scripts/jquery.unobtrusive-ajax*"
                      )
                      )
                        ;
             

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            //for company admin board

            bundles.Add(new StyleBundle("~/assets/css").Include(
                "~/Areas/Company/assets/css/bootstrap.min.css",
                "~/Areas/Company/assets/css/animate.min.css",
               "~/Areas/Company/assets/css/paper-dashboard.css",
               "~/Content/site.css"
                ));

            bundles.Add(new StyleBundle("~/assets/css1").Include(
               "~/Areas/Company/assets/css/themify-icons.css", new CssRewriteUrlTransform()
               ));

            bundles.Add(new StyleBundle("~/assets/css2").Include(
              "~/Areas/Company/assets/css/font-awesome.min.css", new CssRewriteUrlTransform()
              ));



            bundles.Add(new ScriptBundle("~/assets/scripts").Include(
                "~/Areas/Company/assets/js/bootstrap.min.js",
                   "~/Areas/Company/assets/js/bootstrap-notify.js",                   
                   "~/Areas/Company/assets/js/paper-dashboard.js"
                   ));

            
        }
    }
}
