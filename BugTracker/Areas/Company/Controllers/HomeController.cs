﻿using BugTracker.Constants;
using BugTracker.Constants.Enum;
using BugTracker.Helper;
using BugTracker.Helper.Auth;
using BugTracker.Service.Factory;
using BugTracker.Web.Areas.Company.ViewModel;
using BugTracker.Web.Helper; 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AutoMapper;

namespace BugTracker.Web.Areas.Company.Controllers
{
   

    [CustomAuthorize(Roles = ConstantRoleName.COMPANY_ADMIN + "," + ConstantRoleName.COMPANY_OPERATOR)]
    
    public class HomeController : BaseController
    {
        private readonly IPortalServiceBase ServiceFactory;

        private AuthContext authContext;

        public HomeController(IPortalServiceBase _serviceFactory)
        {
            ServiceFactory = _serviceFactory;
            authContext = new AuthContext();
        }


        // GET: Company/Home
        public ActionResult Index()
        {
            UserVM userVM = new UserVM();

            var userResponse = ServiceFactory.GetUserByIdentifier(new Service.Model.UserRequest() { User = new Service.Model.UserSVM() { Identifier = authContext.UserIdentifier } });

            if (userResponse.User != null)
            {
                AccountVM account = new AccountVM();
                Mapper.Map(userResponse.User, account);
                userVM.AccountVM = account;
            }
 
            return View(userVM);
        }

     
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(UserVM userVM)
        {
            var account = userVM.AccountVM;

            var response = ServiceFactory.UpdateBasicDetail(new Service.Model.UserRequest()
                {
                    User = new Service.Model.UserSVM()
                    {
                        Firstname = account.Firstname.Trim(),
                        Lastname = account.Lastname.Trim(),
                        Identifier = authContext.UserIdentifier
                    }
                    ,CompanyName = account.CompanyName.Trim()
                }
            );

           
            if (!response.IsSuccessful)
            {
                ViewBag.updateError = ErrorConstant.ERROR_OCCURED_CANNOT_PROCESS_REQUEST;
            }
            else if (response.CompanyExists)
            {
                ViewBag.updateError = ErrorConstant.COMPANY_NAME_EXISTS;
            }
            else
            {
                ViewBag.updateSuccess = true;
            }

            return View(userVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(UserVM userVM)
        {
            var loginModel = userVM.LoginVM;

            string salt = PasswordEncryptor.CreateSalt512();

            var User = new Service.Model.UserSVM()
            {
                Identifier = authContext.UserIdentifier,
                CurrentPassword = loginModel.CurrentPassword,
                Password = PasswordEncryptor.GenerateHMAC(loginModel.NewPassword.Trim(), salt),
                Salt = salt
            };

            var response = ServiceFactory.ChangePassword(new Service.Model.UserRequest() { User = User });

            if (!response.IsSuccessful)
            {
                ViewBag.passwordError = ErrorConstant.ERROR_OCCURED_CANNOT_PROCESS_REQUEST;
            }
            else if (response.OldPasswordNotMatch)
            {
                ViewBag.passwordError = ErrorConstant.OLD_PASSWORD_DOES_NOT_MATCH;
            }
            else
            {
                ViewBag.passwordSuccess = true;
                loginModel.NewPassword = loginModel.ConfirmPassword = loginModel.CurrentPassword = "";

            }

            return View("Index", userVM);
        }

        

        
        public ActionResult Logout()
        {            
            HttpCookie cookie = new HttpCookie(GeneralConstant.FORMS_AUTH_COOKIE_NAME, "");
            cookie.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie);

            FormsAuthentication.SignOut();
            Session.Abandon();

            return RedirectToAction("Index","Home", new { Area = "" });
        }

        
    }
}