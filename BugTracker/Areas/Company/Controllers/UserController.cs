﻿using AutoMapper;
using BugTracker.Constants;
using BugTracker.Helper.Auth;
using BugTracker.Service.Factory;
using BugTracker.Service.Model;
using BugTracker.Web.Areas.Company.ViewModel;
using BugTracker.Web.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace BugTracker.Web.Areas.Company.Controllers
{
   // [CustomAuthorize(Roles = ConstantRoleName.COMPANY_ADMIN + "," + ConstantRoleName.COMPANY_OPERATOR)]
    public class UserController : BaseController
    {
        private readonly IPortalServiceBase ServiceFactory;

        private AuthContext authContext;

        public UserController(IPortalServiceBase _serviceFactory)
        {
            ServiceFactory = _serviceFactory;
            authContext = new AuthContext();
        }
 
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ClientList(int? page)
        {
            int pagesize = 5;

            List<ClientVM> clientList = new List<ClientVM>();
            

            var test = new ClientVM();
            
            var response = ServiceFactory.GetClientsByCompany(new Service.Model.ClientRequest()
              {
                CompanyIdentifier = authContext.CompanyIdentifier,
                PageSize = pagesize,
                CurrentPageNo = page ?? 1
            }
            );

            if (response.Items != null)
            {
                clientList = Mapper.Map<List<ClientSVM>, List<ClientVM>>(response.Items);
            }

            IPagedList<ClientVM> pageOrders = new StaticPagedList<ClientVM>(clientList,response.CurrentPageNo, pagesize,response.TotalRecords);

            return View(pageOrders);
        }

        public ActionResult EditClient(Guid? identifier)
        {
            ClientVM vm = new ClientVM();
            vm.Identifier = Guid.NewGuid();
            vm.Name = "testing";
            vm.Address = "23 glenbounre walk";

            vm.Roles = GetDropdownValues();
            
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditClient(ClientVM vm)
        {
            vm.Roles = GetDropdownValues();

            return View(vm);
        }

        private List<SelectListItem> GetDropdownValues()
        {
            List<SelectListItem> data = new List<SelectListItem>();

            //data.Add(new SelectListItem() { Value = "0", Text = "--Select--" });
            data.Add(new SelectListItem() { Value = "1", Text = "Role 1" });
            data.Add(new SelectListItem() { Value = "2", Text = "Role 2" });
            data.Add(new SelectListItem() { Value = "3", Text = "Role 3" });

            

            return data;
        }

        [HttpGet]
        
        public ActionResult GetClientList()
        {
            int pagesize = 5;

            List<ClientVM> clientList = new List<ClientVM>();


            var test = new ClientVM();

            var response = ServiceFactory.GetClientsByCompany(new Service.Model.ClientRequest()
            {
                CompanyIdentifier = authContext.CompanyIdentifier,
                PageSize = pagesize,
                CurrentPageNo =   1
            }
            );


            return Json(response.Items, JsonRequestBehavior.AllowGet);
        }

    }
}

