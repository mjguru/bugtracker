﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BugTracker.Web.Areas.Company.ViewModel
{
    public class ClientVM 
    {
        public Guid Identifier { get; set; }

        [Display(Name ="Client Name")]
        [MaxLength(50)]
        [DataType(DataType.Text)]
        [Required]
        public string Name { get; set; }

        public string Address { get; set; }
              
        public DateTime CreatedOn { get; set; }

        public List<SelectListItem> Roles { get; set; }

        [Display(Name = "Role")]
        [Required]
        public int RoleId { get; set; }

        [Display(Name = "Allow Consent")]
        public bool IsChecked { get; set; }

        public EdiVal EditParams { get; set; }
    }

    public struct EdiVal 
    {
        public Guid Identifier { get; set; }

        public string Name { get; set; }
    }
}