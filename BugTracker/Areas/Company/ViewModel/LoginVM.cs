﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BugTracker.Web.Areas.Company.ViewModel
{
    public class LoginVM
    {
        [Display(Name = "Current Password")]
        [Required(ErrorMessage = "Current Password is required")]
        [DataType(DataType.Password)]
        public string CurrentPassword { get; set; }

        [Display(Name = "New Password")]
        [Required(ErrorMessage = "Password is required")]
        [MinLength(8, ErrorMessage = "The field Password must have a minimum length of 8 characters.")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Display(Name = "Confirm Password")]
        [Required(ErrorMessage = "Confirm Password is required")]
        [MinLength(8, ErrorMessage = "The field Confirm Password must have a minimum length of 8 characters.")]
        [DataType(DataType.Password)]
        [Compare("NewPassword")]
        public string ConfirmPassword { get; set; }

    }
}