﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BugTracker.Web.Areas.Company.ViewModel
{
    public class UserVM
    {
        public LoginVM LoginVM { get; set; }

        public AccountVM AccountVM { get; set; }
    }
}