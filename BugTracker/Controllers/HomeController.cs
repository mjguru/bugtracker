﻿using AutoMapper;
using BugTracker.Service.Model;
using BugTracker.Service.Interface;
using BugTracker.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BugTracker.Service;
using BugTracker.Constants;

using BugTracker.Service.Factory;
using BugTracker.Helper; 
using Newtonsoft.Json;
using System.Web.Security;
using BugTracker.Constants.Enum;
using BugTracker.Web.Helper;
using BugTracker.Helper.Auth;

namespace BugTracker.Web.Controllers
{
    
    public class HomeController : BaseController
    {
        private readonly IPortalServiceBase ServiceFactory;

        public HomeController(IPortalServiceBase _serviceFactory)
        {
            ServiceFactory = _serviceFactory;            
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="ReturnUrl"></param>
        /// <returns></returns>
        public ActionResult Index(string ReturnUrl)
        {
            ViewBag.ReturnUrl = ReturnUrl;
            return View();
        }

        public ActionResult AccessDenied()
        {

            return View();
        }

        
        

        

        public ActionResult ForgotPasswordRedirect()
        {


            return View();
        }

        [HttpGet]
        public ActionResult ForgotPassword()
        {
           

            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(ViewModel.UserVM userVM)
        { 
             
            if (userVM != null && userVM.UserRegistrationVM != null)
            {
                var resonse = ServiceFactory.GenerateForgotPasswordToken(new ForgotPasswordTokenRequest() { EmailAddress = userVM.UserRegistrationVM.EmailAddress }).Exists;
                
            }


            return RedirectToAction("ForgotPasswordRedirect");


        }

       [HttpGet]
        public ActionResult ResetPassword(Guid? id)
        {
            ViewModel.UserVM userVM = new ViewModel.UserVM();

            bool hasError = true;

            Guid newGuid;
            if (id != null && id.HasValue && Guid.TryParse(id.ToString(), out newGuid))
            {
                var user = ServiceFactory.CheckForgotPasswordToken(new ForgotPasswordTokenRequest() { Indentifier = id.Value });
                if (user.Exists)
                {
                    hasError = false;

                    userVM.UserRegistrationVM = new UserRegistrationVM();

                    userVM.UserRegistrationVM.Identifier = user.UserIdentifier;
                }
            }


            if (hasError)
            {
                ViewBag.errorMessage = "error";
                
            } 

            return View(userVM);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ViewModel.UserVM userVM)
        { 
            string salt = PasswordEncryptor.CreateSalt512();

            UserResponse response = ServiceFactory.ChangePasswordViaForgotToken(new UserRequest()
            {
                User = new Service.Model.UserSVM()
                {
                    Identifier = userVM.UserRegistrationVM.Identifier,
                    Password = PasswordEncryptor.GenerateHMAC(userVM.UserRegistrationVM.Password.Trim(), salt),
                    Salt = salt
                }
            });

            if (!response.IsSuccessful)
            {
                ViewBag.innerMessage = ErrorConstant.ERROR_OCCURED_CANNOT_PROCESS_REQUEST;
            }
            else
            {
                ServiceFactory.ExpireForgotPasswordToken(new ForgotPasswordTokenRequest() { UserIndentifier = userVM.UserRegistrationVM.Identifier });
                return RedirectToAction("PasswordChangeSuccessful");
            } 
            return View();

        }

        public ActionResult PasswordChangeSuccessful(Guid? id)
        {
            return View();
        }
        

        public ActionResult VerifyAccount(Guid? id)
        {
           
            bool hasError = true;

            Guid newGuid;
            if (id != null && id.HasValue && Guid.TryParse(id.ToString(), out newGuid))
            {                
                bool isAccountValid = ServiceFactory.VerifyUserAccount(new UserRequest() { ActivationLink = id.Value }).Exists;
                if (isAccountValid)
                {
                    hasError = false; 
                }
            }

           
            if (hasError)
            {
                ViewBag.errorMessage =  "error";
            }

        

            return View();
        }

        
 
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult _RegisterPartial(ViewModel.UserVM userVM)
        {            

            if (ModelState.IsValid)
            {
                //first check for duplicate company name
                bool hasError = false;
                List<string> errorMessage = new List<string>(); 

                bool doesCompanyExist = ServiceFactory.GetCompanyByName(new CompanyRequest() { Name = userVM.UserRegistrationVM.CompanyName.Trim() }).Exists;
                if (doesCompanyExist)
                {
                    hasError = true;
                    errorMessage.Add(ErrorConstant.COMPANY_NAME_EXISTS);                   
                }

                bool doesEmailExist = ServiceFactory.GetUserByEmail(new UserRequest() { User = new Service.Model.UserSVM() { EmailAddress = userVM.UserRegistrationVM.EmailAddress.Trim() } }).Exists;
                if (doesEmailExist)
                {
                    hasError = true;
                    errorMessage.Add(ErrorConstant.EMAIL_EXISTS);
                }

                if (hasError)
                {
                    ViewBag.errorMessage = errorMessage;
                }
                else
                {
                    string salt = PasswordEncryptor.CreateSalt512();

                    UserResponse response = ServiceFactory.CreateNewUser(new UserRequest()
                    {
                        User = new Service.Model.UserSVM()
                        {
                            Firstname = userVM.UserRegistrationVM.Firstname.Trim(),
                            Lastname = userVM.UserRegistrationVM.Lastname.Trim(),
                            EmailAddress = userVM.UserRegistrationVM.EmailAddress.Trim(),
                            Password = PasswordEncryptor.GenerateHMAC(userVM.UserRegistrationVM.Password.Trim(), salt)       ,
                            Salt = salt
                        },
                        CompanyName = userVM.UserRegistrationVM.CompanyName.Trim()
                    });
                    
                    if (!response.IsSuccessful)
                    {
                        errorMessage.Add(ErrorConstant.ERROR_OCCURED_CANNOT_PROCESS_REQUEST);
                        ViewBag.errorMessage = errorMessage;
                    }
                    else
                    {
                        return RedirectToAction("RegistrationSuccessful");
                    }
                }                

            }


            return View("Index",userVM);
        }

      


        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(ViewModel.UserVM userVM, string ReturnUrl = "")
        {
            bool isLoginValid = false;
            var response = new UserResponse();

            if (ModelState.IsValid)
            {
                 response = ServiceFactory.ValidateLogin(new UserRequest() { User = new Service.Model.UserSVM() { EmailAddress = userVM.UserLoginVm.EmailAddress.Trim(),
                                    Password = userVM.UserLoginVm.Password.Trim()  } }); 

                if (response.IsSuccessful)
                {
                    if (response.IsAccountLocked)
                    {
                        ViewBag.loginErrorMessage = ErrorConstant.ACCOUNT_LOCKED;
                    }
                    else if (!response.IsValidLogin)
                    {
                        ViewBag.loginErrorMessage = ErrorConstant.INVALID_USERNAME_PASSWORD;
                    }
                    else
                    {
                        isLoginValid = true;
                    }
                }
                else
                {
                   ViewBag.loginErrorMessage = ErrorConstant.ERROR_OCCURED_CANNOT_PROCESS_REQUEST;
                }
            }

            if (isLoginValid)
            {
                SetFormsAuthenticationCookie(response);
                return RedirectUser(response.Role, ReturnUrl);
            }
            else
            {
                return View("Index", userVM);
            }
            
        }

        /// <summary>
        /// 
        /// </summary>
        private ActionResult RedirectUser(string role, string ReturnUrl)
        {
            if (!String.IsNullOrEmpty(ReturnUrl) && ReturnUrl != "/" && Url.IsLocalUrl(ReturnUrl))
            {
                return Redirect(ReturnUrl);
            }
            else
            {
                if (role == Role.CompanyOperator.GetDescription() || role == Role.CompanyAdmin.GetDescription())
                {
                    return RedirectToAction("Index", "Home", new { Area = "Company" });
                }
                else if (role == Role.Client.GetDescription())
                {
                    return RedirectToAction("Index", "Home", new { Area = "Client" });
                }
            }

           

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="response"></param>
        private void SetFormsAuthenticationCookie(UserResponse response)
        {
            AuthenticationModel userModel = new AuthenticationModel()
            {
                UserIdentifier = response.Identifier,
                FirstName = response.Firstname,
                LastName = response.Lastname,
                RoleName = new List<string>() { response.Role },
                CompanyIdentifier = response.CompanyIdentifier,
                CompanyName = response.CompanyName
            };

            string userData = JsonConvert.SerializeObject(userModel);
            int timeout = 287877;

            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket
                (
                    1, response.EmailAddress, DateTime.Now, DateTime.Now.AddMinutes(timeout), true, userData
                );

            string enTicket = FormsAuthentication.Encrypt(authTicket);
            //FormsAuthentication.SetAuthCookie("mj", true);
            HttpCookie faCookie = new HttpCookie(GeneralConstant.FORMS_AUTH_COOKIE_NAME, enTicket);
            faCookie.Expires = DateTime.Now.AddMinutes(timeout);

            Response.Cookies.Add(faCookie);
        }

        public ActionResult RegistrationSuccessful()
        {

            return View();
        }

      
        public ActionResult ContactUs()
        {

            return View();
        }

    }

}
