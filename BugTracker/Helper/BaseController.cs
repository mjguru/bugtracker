﻿

using BugTracker.Helper;
using BugTracker.Helper.Auth;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BugTracker.Web.Helper
{
    public class BaseController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestContext"></param>
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);

            var authContext = new AuthContext();

            ViewBag.LoggedInUserName = $"Welcome {authContext.FirstName} {authContext.LastName} (Your Role: { authContext.Role})";

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnException(ExceptionContext filterContext)
        {
            string error = filterContext.Exception.ToString();

            if (filterContext.Exception.InnerException  != null)
            {
                error += " Inner Exception: " + filterContext.Exception.InnerException.ToString();
            }

            Logger.LogError(error);


            RedirectToRouteResult routeData = null;
 
                routeData = new RedirectToRouteResult
                    (new System.Web.Routing.RouteValueDictionary
                    (new
                    {
                        controller = "Shared",
                        action = "Error",
                        Area = ""
                    }
                    ));
           

            filterContext.ExceptionHandled = true;

            filterContext.Result = routeData;
        }
    }

}