﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BugTracker.Web.Helper
{
    public class UiHelper
    {

        public static string GetCurrentMenuCss(string action, string controller)
        {
            string css = "";

            if (Controller().ToString().ToLower().Trim() == controller.ToLower().Trim() && ActionName().ToString().ToLower().Trim() == action.ToLower().Trim())
            {
                css = "active";
            }

            return css;
        }

        private static string Controller()
        {
            var routeValues = HttpContext.Current.Request.RequestContext.RouteData.Values;

            if (routeValues.ContainsKey("controller"))
                return (string)routeValues["controller"];

            return string.Empty;
        }

        private static string ActionName()
        {
            var routeValues = HttpContext.Current.Request.RequestContext.RouteData.Values;

            if (routeValues.ContainsKey("action"))
                return (string)routeValues["action"];

            return string.Empty;
        }
    }
}