﻿using AutoMapper;
using BugTracker.Service.Model;
using BugTracker.Web.Areas.Company.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BugTracker.Web.Mapping
{
    public class EntityToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "EntityToViewModelMapping"; }
        }

        public EntityToViewModelMappingProfile()
        {
            //CreateMap<User, AccountVM>()
            //    .ForMember(g => g.EmailAddress, map => map.MapFrom(vm => vm.EmailAddress))
            //    .ForMember(g => g.CompanyName, map => map.MapFrom(vm => vm.CompanyName))
            //    .ForMember(g => g.Firstname, map => map.MapFrom(vm => vm.Firstname))
            //    .ForMember(g => g.Lastname, map => map.MapFrom(vm => vm.Lastname)) 
            //    ;

            CreateMap<Service.Model.UserSVM, AccountVM>();
            CreateMap<Service.Model.ClientSVM, Areas.Company.ViewModel.ClientVM>();
        }

    }
}