﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
 
namespace BugTracker.Web.ViewModel
{
    public class UserVM
    {
        public UserLoginVM UserLoginVm { get; set; }

        public UserRegistrationVM UserRegistrationVM { get; set; }


    }
}