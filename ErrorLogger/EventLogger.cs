﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErrorLogger
{
    public class EventLogger : IEventLogger
    {
        public void LogErrorMessage(string message)
        {
            var logger = NLog.LogManager.GetLogger("nLogErrorLogger");
            logger.Error(message);
        }

        public void LogInformation(string message)
        {
            var logger = NLog.LogManager.GetLogger("nLogGeneralInfoLogger");
            logger.Info(message);
        }


    }
}
