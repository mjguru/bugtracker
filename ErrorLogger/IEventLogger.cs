﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErrorLogger
{
    public interface IEventLogger
    {
        void LogErrorMessage(string message);
        void LogInformation(string message);
    }
}
