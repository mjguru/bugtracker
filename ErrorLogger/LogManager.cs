﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErrorLogger
{
    public class LogManager
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="errorMessage"></param>
        public static void LogError(string errorMessage)
        {
            IEventLogger logger = new EventLogger();
            logger.LogErrorMessage(errorMessage);
        }

        public static void LogInformation(string errorMessage)
        {
            IEventLogger logger = new EventLogger();
            logger.LogInformation(errorMessage);
        }
    }
}
